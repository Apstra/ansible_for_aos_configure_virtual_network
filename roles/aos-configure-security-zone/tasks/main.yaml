---
# ----------------------------------------------
# Load intent and process it, to prepare API request
# ----------------------------------------------
  - name: "{{ file_name }} - Load intent file" 
    include_vars:
      file: "{{ item }}"
      name: sz_intent_object
    check_mode: no

# ----------------------------------------------
# Create the body structure for the API call
# ----------------------------------------------
  - name: "{{ file_name }} - Create body structure"
    set_fact:
      request_body: "{{ sz_intent_object | combine(new_keys) }}"
    vars:
      new_keys:
        label: "{{ sz_intent_object.vrf_name }}"
        sz_type: "{{ sz_intent_object.sz_type|default('evpn') }}"
    check_mode: no

# ----------------------------------------------
# Check presence of the SZ
#   If  yes  Then  use PUT   (Update)
#   else           use POST  (Create)
# ----------------------------------------------
  - name: "{{ file_name }} - Get security_zone_id if exists"
    set_fact:
      security_zone_id: "{{ security_zones | json_query(query_sz_id) | first }}"
    when: sz_intent_object.vrf_name in security_zones | json_query(query_sz_labels)
    vars:
      query_sz_id: "json.items.*.{label: label, id:id}[?label==\
        '{{ sz_intent_object.vrf_name }}'].id"
      query_sz_labels: "json.items.*.label"
    ignore_errors: "{{ ansible_check_mode }}"

# ----------------------------------------------
# Push request to AOS
#   Opertion = Update -> method = PUT
# ----------------------------------------------
  - name: "{{ file_name }} - Update Security Zone (PUT API call)"
    uri:
      url: "{{ aos_api_url }}/blueprints/{{ blueprint_id }}/security-zones/\
        {{ security_zone_id }}"
      method: PUT
      status_code: 204
      validate_certs: no
      headers: "{{ aos_api_headers }}"
      body: "{{ request_body }}"
      body_format: json
    when: 
      - sz_intent_object.vrf_name in security_zones | json_query(query_sz_labels)
      - security_zone_id is defined
    vars:
      query_sz_labels: "json.items.*.label"
    register: security_zone
    ignore_errors: yes
    changed_when:
      - security_zone.status == 204


# ----------------------------------------------
# Push request to AOS
#   Opertion = Create -> method = POST
# ----------------------------------------------
  - name: "{{ file_name }} - Create Security Zone (POST API call)"
    uri:
      url: "{{ aos_api_url }}/blueprints/{{ blueprint_id }}/security-zones"
      method: POST
      status_code: 201
      validate_certs: no
      headers: "{{ aos_api_headers }}"
      body: "{{ request_body }}"
      body_format: json
    when: sz_intent_object.vrf_name not in security_zones | json_query(query_sz_labels)
    vars:
      query_sz_labels: "json.items.*.label"
    register: security_zone
    ignore_errors: yes
    changed_when:
      - security_zone.status == 201


  - name: "{{ file_name }} - Get security_zone_id"
    set_fact:
      security_zone_id: "{{ security_zone | json_query(query) }}"
    when: sz_intent_object.vrf_name not in security_zones | json_query(query_sz_labels)
    vars:
      query_sz_labels: "json.items.*.label"
      query: "json.id"
    check_mode: no

# ----------------------------------------------
# Assign Resources
# ----------------------------------------------
  - name: "{{ file_name }} - Assign resources"
    uri:
      url: "{{ aos_api_url }}/blueprints/{{ blueprint_id }}/resource_groups/\
        ip/sz%3A{{ security_zone_id }}%2C{{ group_name }}"
      method: PUT
      status_code: 202
      validate_certs: no
      headers: "{{ aos_api_headers }}"
      body: 
        pool_ids: "{{ pool_ids }}"
      body_format: json
    loop:
      - "leaf_l3_peer_link_link_ips"
      - "leaf_loopback_ips"
    loop_control:
      loop_var: group_name
    when: sz_intent_object.vrf_name not in security_zones | json_query(query_sz_labels)
    vars:
      query_sz_labels: "json.items.*.label"
    changed_when:
      - security_zone.status == 202
