# Infrastructure-as-Code with AOS
Branches:
- Master:  [![pipeline status](https://gitlab.com/mab1/aos_iac/badges/master/pipeline.svg)](https://gitlab.com/mab1/aos_iac/commits/master)
- Develop:  [![pipeline status](https://gitlab.com/mab1/aos_iac/badges/develop/pipeline.svg)](https://gitlab.com/mab1/aos_iac/commits/develop)

----

## What this repo is about?
- This repository contains examples on how to programmatically use Apstra's AOS, via its northbound API.
- In this repository, you will see how to:
  1. Identify API endpoints. AOS provides a unified, declarative and vendor-agnostic API.
  2. Group them in an Ansible Playbook to create a workflow for achieving a particular task. The example here focuses on Day-2 operations like creating/updating virtual networks, it assumes already deployed blueprint.
  3. Use Continuous Integration/Continuous Delivery practices to embrace Infrastructure-as-Code approach. The high-level idea is to treat the infrastructure intent as Code, by representing it in a source control system, linked to a CI tool that automatically test the code and push to production upon a commit push (by using the previously defined Ansible playbooks).
  

## What tools are used ?
- **Ansible**: Contains playbooks that make API calls to AOS server, via CRUD operations and some processing on the received data. Ansible roles are defined to allow re-usability.
- **Docker**: To package Ansible, along with required dependencies as well as some basic testing utilities (used in CI/CD pipelines) in a docker image. The image is available through the docker registry (Docker-hub.
- **Gitlab** and **Gitlab-CI**: Gitlab is a version control system. It has an integrated CI/CD tool [Gitlab-CI](https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/). In this repo, the CI/CD pipelines execute Ansible playbooks.


## How to use this repo in your environment ?
  - Documentation Wiki pages are [here](https://gitlab.com/Apstra/ansible_for_aos_configure_virtual_network/-/wikis/home). It is broken down in three pages.
    - Step 1: Run the Ansible playbooks. [Link](https://gitlab.com/Apstra/ansible_for_aos_configure_virtual_network/-/wikis/documentation/1.-Run-Ansible-playbooks).
    - Step 2: Learn how to contribute to writing Roles and/or Playbooks. [Link](https://gitlab.com/Apstra/ansible_for_aos_configure_virtual_network/-/wikis/documentation/2.-Write-playbook-roles).
    - Step 3: Learn how to embrace CI/CD tools and practices to automatically trigger palybooks execution and define delivery pipelines. [Link](https://gitlab.com/Apstra/ansible_for_aos_configure_virtual_network/-/wikis/documentation/3.-Continuous-Integration,-Continuous-Delivery).
      - At a high level:
        1. Fork the repository to your own Gitlab account.
        2. Install Gitlab-CI runner on the AOS server.
        3. Register it to your Gitlab repository.
        4. Understand the branching strategy used in the Gitlab-CI pipeline
        3. Test and have fun.
